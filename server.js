// server.js
// Copyright (C) 2018 Kaz Nishimura
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL-3.0-or-later

"use strict";

// The Application Insights SDK must be loaded as early as possible.
if ("APPINSIGHTS_INSTRUMENTATIONKEY" in process.env) {
    const appinsights = require("applicationinsights");
    appinsights.setup().start();
}

const http = require("http");
const dispatch = require("./lib/dispatch");

var dispatcher = new dispatch.Dispatcher();

var server = http.createServer(dispatcher.dispatch.bind(dispatcher));
server.listen(process.env["PORT"]);
