# Description

This program implements an experimental web application on Node.js.

See the [repository wiki][wiki] for more information.

[![(License)](https://img.shields.io/badge/license-AGPL--3.0--or--later-blue.svg)][AGPL-3.0]
[![(Open issues)](https://img.shields.io/bitbucket/issues/linuxfront/webapp2-azure.svg)][open issues]
[![(Build status)](https://linuxfront-functions.azurewebsites.net/api/bitbucket/build/linuxfront/webapp2-azure?branch=master)][pipelines]

[AGPL-3.0]: https://opensource.org/licenses/AGPL-3.0

[Wiki]: https://bitbucket.org/linuxfront/webapp2-azure/wiki
[Open issues]: https://bitbucket.org/linuxfront/webapp2-azure/issues?status=new&status=open
[Pipelines]: https://bitbucket.org/linuxfront/webapp2-azure/addon/pipelines/home
